#!/bin/bash 

#module load GDAL/2.2.0-intel-2018

#cd /home/ddare/OneDrive/luckynet/Data/treeCover_hansen
cd /home/ddare/Data/nextcloud/pelican_home/treeCover_hansen
#cd /home/ddare/Data/nextcloud/pelican_home/ESA_CCI

ls

#check resolution 
gdalinfo Hansen_GFC-2018-v1.6_treecover2000_00N_040W.tif

#create list of file 
dir *.tif > hansen_list.txt

#mosaic the rasters (ref: http://dominoc925.blogspot.com/2015/11/mosaic-large-number-of-geotiff-images.html)
gdalwarp -multi -wm 1000 --optfile hansen_list.txt hansen_mosaic.tif

#cut the mosaic raster
gdalwarp -te -88 -66 -25 5 hansen_mosaic.tif hansen_mosaic_crop.tif

#the mosaic raster is too big, we have to resample it 
gdalwarp -tr 0.008333333300000 -0.008333333300000  -r near hansen_mosaic_crop.tif resampled_hansen_mosaic.tif


#SA_ext<-extent(-88, -25, -66, 5 ) method ngb
#1k res in WGS84 0.008333333333330000814,-0.008333333333330000814
#0.008333333300000,-0.008333333300000


#reproject raster to equal area projection
#clipper.tif sarà il rastere che utilizzero' sia per fare la riproiezione che per creare l'extent da utilizzare per fare il crop
gdaltindex clipper.shp clipper/dem.tif

gdalwarp -t_srs '+proj=laea +lat_0=5 +lon_0=19 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs' hansen_mosaic.tif hansen_mosaic_laea.tif





##crop the mosaic raster based on the extension of the study area 
# ref http://joeyklee.github.io/broc-cli-geo/guide/XX_raster_cropping_and_clipping.html
# https://gis.stackexchange.com/questions/125202/gdal-clipping-a-raster-with-another-raster
#https://www.geos.ed.ac.uk/~smudd/TopoTutorials/html/tutorial_raster_conversion.html

#gdalwarp -cutline clipper.shp -crop_to_cutline hansen_mosaic_laea.tif hansen_mosaic_laea_crop.tif



####esa cci processing 

#73.9855346679686932,-55.0613136291503835 : -28.8390522003173189,5.2648777945952361

#crop esa cci  (xmin, ymin, xmax, ymax: (-88, -66, -25, 5 )
#gdalwarp -te -88 -66 -25 5 ESACCI-LC-L4-LCCS-Map-300m-P1Y-2000-v2.0.7.tif ESA_CCI2000_crop.tif

#reproject it 
#gdalwarp -t_srs '+proj=laea +lat_0=5 +lon_0=19 +x_0=0 +y_0=0 +ellps=WGS84 +units=m +no_defs' ESA_CCI2000_crop.tif ESA_CCI2000_laea.tif

#use a raster id 
# ok i think we should do part of this in R
#https://stackoverflow.com/questions/47660951/how-to-automatically-convert-many-fields-of-a-polygon-shapefile-to-raster-in-r

#create unique ID grid cell, rasterize and polygonize it 
#use the polygon to extract the zonal stat

#Rscript setwd(dir = "OneDrive/luckynet/Data/")   
#Rscript library(raster)
#Rscript SA_ext<-extent(-88, -25, -66, 5 )
#Rscript bio1<-raster("CHELSA_bio10_01_land/CHELSA_bio10_1.tif")
#Rscript bio1<-raster::crop(bio1, SA_ext)
#Rscript r_id<-raster(vals= seq(1, ncell(r_id)), ext = extent(bio1), res=res(bio1), crs=  crs(bio1))
#Rscript writeRaster(r_id, "rast_ID_1k_SA.tif" )

gdal_polygonize.py rast_ID_1k_SA.tif -f "ESRI Shapefile" grid_ID_1k_SA.shp

ogrinfo -so grid_ID_1k_SA.shp -sql "SELECT * FROM grid_ID_1k_SA"



ogrinfo rast_ID_1k_SA_masked.shp -sql "ALTER TABLE rast_ID_1k_SA_masked ADD [COLUMN] ID_field integer"


gdal_polygonize.py rast_ID_1k_SA_masked.tif -f "ESRI Shapefile" grid_ID_1k_SA_mask.shp







pkextractogr -i ESA_CCI2000_crop.tif -s grid_ID_1k_SA.shp -r count -f "ESRI Shapefile" -o extracted.shp

pkextractogr -i ESA_CCI2000_crop.tif -s rast_ID_1k_SA_masked.tif -r count -f "ESRI Shapefile" -o extracted.shp


exactextract -p /home/ddare/OneDrive/luckynet/gdalBash_test/Response/cat00_spatiaCV_training.shp -r ESA_CCI2000_crop.tif -f CODE -s count -o exact_zonStat.csv

exactextract -p /home/ddare/OneDrive/luckynet/gdalBash_test/Response/cat00_spatiaCV_training.shp -r ESA_CCI2000_crop.tif -f CODE -s count -o exact_zonStat.csv



gdalbuildvrt -overwrite  -separate LC_stack.vrt LC*.tif
#pkextractogr -i ESA_CCI2000_crop.tif -s rast_ID_1k_SA_masked.tif -r count -f "ESRI Shapefile" -o extracted.shp

pkextractimg -i ESA_CCI2000_crop.tif -s rast_ID_1k_SA_masked.tif -o extracted2.sqlite





exactextract -p grid_500km.shp -r ESA_CCI2000_crop.tif -f CODE -s count -o exact_zonStat.csv





#https://gis.stackexchange.com/questions/158338/extracting-the-pixelwise-proportion-of-classes-of-a-finer-raster-in-a-coarser-on
gdalbuildvrt -overwrite  -separate LC_stack.vrt LC_*.tif

gdalwarp -ot Float32 -tr 1000 1000 -r average bit_raster.tif average_1km.tif
gdalwarp -overwrite -ot Float32  -s_srs EPSG:4326 -tr 0.008333333300000 0.008333333300000 -r average LC_stack.vrt average_1km.tif




Rscript setwd(dir = "OneDrive/luckynet/Data/")   #
Rscript library(raster)
Rscript r_id<-raster(rast_ID_1k_SA.tif)
Rscript pols<-shapefile(rast_ID_1k_SA.tif)

Rscript x <- subs(r_id, data.frame(extracted), by='id', which=2:ncol(extracted), filename="rstr.grd")



##""




#ESA_CCI2000_crop.tif



#export YEAR=$(expr $SLURM_ARRAY_TASK_ID + 1991 ) 

#echo 10 11 12 20 30 40 50 60 61 62 70 71 72 80 90 100 110 120 121 122 130 140 152 153 160 170 180 190 200 201 202 210 220 | xargs -n 1 -P 4 bash -c $' CLASS=$1
#pkgetmask -ot Byte -co COMPRESS=DEFLATE -co ZLEVEL=9 -min  $( echo $CLASS - 0.5 | bc ) -max $( echo $CLASS + 0.5 | bc ) -data 1 -nodata 0   -i ESA_CCI2000_crop.tif -o LC${CLASS}_Y.tif'
#gdal_edit.py  -a_ullr 0.002777777777777  -0.002777777777777 LC${CLASS}_Y.tif' _



