#!/bin/bash

#create continent array  arrays
CONTINENT=("SouthAmerica" "Africa" "NorthAmerica" "Europe" "Asia" "Oceania")
# echo ${CONTINENT[@]}

# PATH=("/home/ddare/Desktop/Chris_codes/BCK_BRUXELLES/crop_test")


# crop loop through each continent
for ai in "${CONTINENT[@]}"; do

      echo 'Processing' $ai
      mkdir ${ai}
        
          for i in *.tif; do 
          out_name=$i
          # echo ${ai}
          echo "INPUT" ${i}
          # echo "OUTPUT" ${out_name}
          # echo ${ai}/${ai}_PolyExtent_latlon.shp
          # gdalinfo ${i}
          gdalwarp -overwrite -co "COMPRESS=LZW" -cutline  ${ai}/${ai}_PolyExtent_latlon.shp -crop_to_cutline ${i} ${ai}/${out_name}
          done
done


# gdalwarp -overwrite -co "COMPRESS=LZW" -cutline Africa/Africa_PolyExtent_latlon.shp -crop_to_cutline templ_co_2000_prec_mean.tif Africa/templ_co_2000_prec_mean.tif

# mkdir fold{2000..2015}
# mv *_{2000..2001}.tif fold{2000..2001}/.
