#!/bin/bash

#module load old
#module load GDAL/2.2.0-intel-2018  

#gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear global_GPP_TS/2000_GPP_global_1k.tif global_GPP_TS_10k/2000_GPP_global_10k.tif


#YEAR=`echo {2001..2016}`
#YEAR={2001...2016}
#YEAR=$(seq 2001 2016)
cd 1k/
for i in *.tif; do
		echo ${i}
		in_name=`echo ${i} | cut -f 1 -d '.'`
		echo "processing " ${in_name}
		gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear ${i} /storepelican/ddare/elevANDmasks/10k/${in_name}_10k.tif
done

