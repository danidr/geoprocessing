#!/bin/bash

#module load old
#module load GDAL/2.2.0-intel-2018  

#for i in temp_annual_mean/*; do
#		echo ${i}
#		in_name=`echo ${i} | cut -d '/' -f 2`
#		echo $in_name
#		in_name2=`echo ${in_name} | cut -d '_' -f 1`
#		echo $in_name2
#		gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear ${i} temp_annual_mean_10k/${in_name2}_temp_annual_mean_10k.tif
#done

for i in prec_annual_mean/*.tif; do
         echo ${i}
         in_name=`echo ${i} | cut -d '/' -f 2`
         echo $in_name
         in_name2=`echo ${in_name} | cut -d '_' -f 1`
         echo $in_name2
         gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear ${i} prec_annual_mean_10k/${in_name2}_prec_annual_mean_10k.tif
done
          
