#!/bin/bash 
module purge
module load pktools

####CHELSA ref https://www.nature.com/articles/sdata2017122

#############################################
########### DOWNLOAD TEMPERATURES ###########
#############################################
# cd /temp_monthly_mean/ 
# for YEAR in {2000..2013}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries/tmean/CHELSA_tmean_${YEAR}_${i_month}_V1.2.1.tif 
# 		done
# done 	


# #download from chelsa crust for the year 2014 to 2016
# for YEAR in {2014..2016}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries20c/prec/CHELSAcruts_prec_${i_month}_${YEAR}_V.1.0.tif
# 		done
# done 	



#####################################################
########### get annual mean  TEMPERATURES ###########
#####################################################
for YEAR in {2000..2013}; do 
	echo $YEAR
	echo "building VRT of" $YEAR
	gdalbuildvrt -overwrite -separate temporary_files/${YEAR}_tmean.vrt temp_monthly_mean/CHELSA_tmean_${YEAR}_*.tif
	echo "computing the annual mean of" $YEAR
	pkstatprofile -nodata '-9999' -i temporary_files/${YEAR}_tmean.vrt -o temp_annual_mean/${YEAR}_Temp_mean.tif -f mean	
done
echo "remove temporary files"
rm -f temporary_files/*.vrt
