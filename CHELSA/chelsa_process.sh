#!/bin/bash 
module purge
module load pktools

####CHELSA ref https://www.nature.com/articles/sdata2017122

#############################################
########### DOWNLOAD TEMPERATURES ###########
#############################################
# cd /temp_monthly_mean/ 
# for YEAR in {2000..2013}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries/tmean/CHELSA_tmean_${YEAR}_${i_month}_V1.2.1.tif 
# 		done
# done 	


# #download from chelsa crust for the year 2014 to 2016
# for YEAR in {2014..2016}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries20c/prec/CHELSAcruts_prec_${i_month}_${YEAR}_V.1.0.tif
# 		done
# done 	

# ###############################################
# ########### DOWNLOAD PRECIPITATIONS ###########
# ###############################################
# cd /prec_monthly_mean/ 
# for YEAR in {2000..2013}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries/prec/CHELSA_prec_${YEAR}_${i_month}_V1.2.1.tif 
# 		done
# done 

# #download from chelsa crust for the year 2014 to 2016
# for YEAR in {2014..2016}; do 
# 	echo $YEAR
# 	declare -a month=("01" "02" "03" "04" "05" "06" "07" "08" "09" "10" "11" "12")
# 		for i_month in "${month[@]}"	; do
# 		echo "download monthly T means of year" $YEAR "month" $i_month
# 		wget https://www.wsl.ch/lud/chelsa/data/timeseries20c/temp/CHELSAcruts_prec_${i_month}_${YEAR}_V.1.0.tif
# 		done
# done 	


#############################################
########### get annual mean  TEMPERATURES ###########
#############################################
for YEAR in {2000..2013}; do 
	echo $YEAR
	echo "building VRT of" $YEAR
	gdalbuildvrt -overwrite -separate temporary_files/${YEAR}_tmean.vrt temp_monthly_mean/CHELSA_tmean_${YEAR}_*.tif
	#exit 1
	echo "computing the annual mean of" $YEAR
	pkstatprofile -nodata '-9999' -i temporary_files/${YEAR}_tmean.vrt -o temp_annual_mean/${YEAR}_Temp_mean.tif -f mean	
	echo "remove temporary files"
	rm -f temporary_files/*.vrt
done





# for i  in temp_monthly_mean/*.tif ; do
# 			in_name=`echo ${i} | cut -d '/' -f 2`
# 			# echo $in_name
# 			for YEAR in {2014..2016}; do 
# 			echo $YEAR
# 			echo "building VRT of tiles" $in_name
# 			gdalbuildvrt -overwrite -separate temporary_files/${YEAR}_${in_name}.vrt ${i_folder}/*.tif
# 			# exit 1 #to stop the script

# 		echo "computing the annual mean of tiles" $in_name
# 		pkstatprofile -nodata 65535 -i temp/${YEAR}_${in_name}_tile.vrt -o annual_mean/${YEAR}_${in_name}_mean.tif -f mean	
# 		#remove temporary files
# 		rm -rf gpp_${YEAR}
# 		rm -f temp/${YEAR}_${in_name}_tile.vrt
# 			done
# echo "computing the global merge of year" $YEAR
# gdalwarp -overwrite -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata -9999 annual_mean/${YEAR}_*.tif global_GPP_TS/${YEAR}_GPP_global.tif
# #remove temporary files
# rm -f annual_mean/${YEAR}_*.tif

# done
