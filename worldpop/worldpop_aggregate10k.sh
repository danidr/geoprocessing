#!/bin/bash

#module load old
#module load GDAL/2.2.0-intel-2018  

for i in worldpop_global_2000_2020/*; do
		echo ${i}
		in_name=`echo ${i} | cut -d '/' -f 2`
		echo $in_name
		in_name2=`echo ${in_name} | cut -d '_' -f 2`
		echo $in_name2
		gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear ${i} worldpop_global_2000_2020_10k/${in_name2}_ppp_10k.tif
done

