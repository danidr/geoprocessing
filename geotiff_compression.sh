#!/bin/bash

#compression info https://gis.stackexchange.com/questions/1104/should-gdal-be-set-to-produce-geotiff-files-with-compression-which-algorithm-sh

gdalinfo templ_2000_prec_mean.tif 
gdal_edit.py -a_nodata 65535 templ_2000_prec_mean.tif 
gdalinfo templ_2000_prec_mean.tif 

gdal_translate -co "COMPRESS=LZW" templ_2000_prec_mean.tif templ_co_2000_prec_mean.tif 
gdal_translate  -co COMPRESS=DEFLATE -co ZLEVEL=9 templ_2000_prec_mean.tif templ_co2_2000_prec_mean.tif 

ls


for i in *.tif; do
	out_name=$i
	echo $out_name
	# gdal_translate -co "COMPRESS=LZW" $i comp/$out_name
done



