#!/bin/bash 
#http://erouault.blogspot.com/2011/12/seamless-access-to-remote-global-multi.html
#example https://github.com/selvaje/YaleRep/tree/master/GPP
#scp zhang_prepocess/zhang_prepocess_test.sh ddare@pelican:/storepelican/ddare/Zhang_GPP_processing/


#module purge
#module load pktools


####################################################
############### 1. download dataset ################
####################################################
# for YEAR in {2000..2016}; do 
# wget http://hs.pangaea.de/Maps/GPPVPM/gpp_${YEAR}.zip
# done

####################################################
############## 2. compute annual mean  #############
####################################################

# for i_folder in gpp_2000/* ; do
# 	in_name=`echo ${i_folder} | cut -d '/' -f 2`
# 	# echo $in_name
# 	echo "building VRT of tiles" $in_name
# 	gdalbuildvrt -overwrite -separate temp/${in_name}_tile.vrt ${i_folder}/*.tif
# 	echo "computing the annual mean of tiles" $in_name
# 	pkstatprofile -nodata 65535 -i temp/${in_name}_tile.vrt -o annual_mean/${in_name}_2000_mean.tif -f mean	
# done
# rm -f temp/*.vrt

####################################################
############## 3. assemble all the tiles ###########
####################################################
# for YEAR_tiles in {2000..2016}; do
# 	gdalwarp -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata 65535 annual_mean/*_${YEAR_tiles}_mean.tif global_GPP_TS/${YEAR_tiles}_GPP_global.tif
# done 
# rm -f annual_mean/*_mean.tif




####################################################
############## BIG PROCESSING ######################
####################################################
for YEAR in {2006..2016}; do 
echo "download GPP of year" $YEAR
wget http://hs.pangaea.de/Maps/GPPVPM/gpp_${YEAR}.zip
unzip gpp_${YEAR}.zip
rm -rf gpp_${YEAR}.zip
	for i_folder in gpp_${YEAR}/* ; do
		#echo ${i_folder}
		in_name=`echo ${i_folder} | cut -d '/' -f 2`
		#echo $in_name
		echo "building VRT of tiles" $in_name
		gdalbuildvrt -overwrite -separate temp/${YEAR}_${in_name}_tile.vrt ${i_folder}/*.tif
		echo "computing the annual mean of tiles" $in_name
		pkstatprofile -co COMPRESS=LZW -nodata 65535 -i temp/${YEAR}_${in_name}_tile.vrt -o annual_mean/${YEAR}_${in_name}_mean.tif -f mean	
		done
echo "computing the global merge of year" $YEAR
gdalwarp -overwrite -co "COMPRESS=LZW" -ot Float32 -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata -9999 annual_mean/${YEAR}_*.tif global_GPP_TS/${YEAR}_GPP_global_test.tif
#remove temporary files
echo "remove temporary files"
rm -f temp/${YEAR}_${in_name}_tile.vrt
rm -f annual_mean/${YEAR}_*.tif
rm -rf gpp_${YEAR}
done













####################################################



#I did some test on these folders
#fold_list="h10v02" "h10v03" "h10v04" 
#echo ${fold_list[*]} 
#locally it works
#gdalbuildvrt -overwrite -separate h10v02_tile.vrt h10v02/*.tif
#pkstatprofile -nodata 65535 -i h10v02_tile.vrt -o h10v02_2000_mean_profile.tif -f mean	


#assemble tiles test

# gdalwarp -srcnodata 65535 -dstnodata 9999 annual_mean/*_mean.tif global_GPP_TS/2000_GPP_global.tif
#dir annual_mean/*.tif > 2000_list.txt

#mosaic the rasters (ref: http://dominoc925.blogspot.com/2015/11/mosaic-large-number-of-geotiff-images.html)
#gdalwarp -multi -wm 2000 --optfile 2000_list.txt global_GPP_TS/2000_mosaic.tif
# gdalwarp -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata -9999 annual_mean/*_mean.tif global_GPP_TS/2000_GPP_global_1k_bis.tif
# gdalwarp -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata -9999 2000_GPP_global.tif 2000_GPP_global_1k_bis.tif
# gdalwarp -t_srs EPSG:4326 -tr 0.05 -0.05 -r bilinear -srcnodata 65535 -dstnodata -9999 2000_GPP_global.tif 2000_GPP_global_005deg.tif
# gdalwarp -t_srs EPSG:4326 -tr 0.5 -0.5 -r bilinear -srcnodata 65535 -dstnodata -9999 2000_GPP_global.tif 2000_GPP_global_05deg.tif

# gdalwarp -tr 0.05 -0.05 -r bilinear 2000_GPP_global_1k.tif 2000_GPP_global_005deg.tif
# gdalwarp -tr 0.5 -0.5 -r bilinear 2000_GPP_global_1k.tif 2000_GPP_global_05deg.tif
#scp  ddare@pelican:/storepelican/ddare/Zhang_GPP_processing/global_GPP_TS/2000_GPP_global_1k.tif /home/ddare/working_files

# pkgetmask -ot Byte -co COMPRESS=DEFLATE -co ZLEVEL=9 -min 6000 -data 1 -nodata 0   -i 2000_GPP_global_1k.tif -o 2000_GPP_global_1k_masked.tif
