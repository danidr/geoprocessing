#!/bin/bash

#module load old
#module load GDAL/2.2.0-intel-2018  

#gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear global_GPP_TS/2000_GPP_global_1k.tif global_GPP_TS_10k/2000_GPP_global_10k.tif


#YEAR=`echo {2001..2016}`
#YEAR={2001...2016}
#YEAR=$(seq 2001 2016)


cd  global_GPP_TS/ 
for YEAR in {2001..2016}; do
		echo ${YEAR}_GPP_*.tif
		#in_name=`echo ${YEAR}_GPP_*.tif | cut -d '_' -f 2`
		gdalwarp -co "COMPRESS=LZW"  -tr 0.08333333300000 -0.08333333300000 -r bilinear ${YEAR}_GPP_*.tif  ${YEAR}_GPP_global_10k.tif
done

mv *_10k.tif global_GPP_TS_10k/. 


