#!/bin/bash 
module purge
module load pktools

wget http://hs.pangaea.de/Maps/GPPVPM/gpp_2004.zip

unzip gpp_2004.zip

rm -rf gpp_2004.zip


####################################################
############## 2. compute annual mean  #############
####################################################
 
 for i_folder in gpp_2004/* ; do
 	 	in_name=`echo ${i_folder} | cut -d '/' -f 2`
 	 		#echo $in_name
 	 		 	echo "building VRT of tiles" $in_name
 	 		 	    gdalbuildvrt -overwrite -separate temp/2004_${in_name}_tile.vrt ${i_folder}/*.tif
 	 		 	        echo "computing the annual mean of tiles" $in_name
 	 		 	           pkstatprofile -co COMPRESS=LZW -nodata 65535 -i temp/2004_${in_name}_tile.vrt -o annual_mean/2004_${in_name}_mean.tif -f mean 
 	 		 	           done


 	 		 	           rm -f temp/*.vrt

 	 		 	           echo "computing the global merge of year" $YEAR
 	 		 	           gdalwarp -overwrite -co "COMPRESS=LZW" -ot Float32 -t_srs EPSG:4326 -tr 0.008333333300000 -0.008333333300000 -r bilinear -srcnodata 65535 -dstnodata -9999 annual_mean/2004_*.tif global_GPP_TS/2004_GPP_global_test.tif

#remove temporary files
echo "remove temporary files"
rm -f temp/2004_${in_name}_tile.vrt
rm -f annual_mean/2004_*.tif
rm -rf gpp_2004

