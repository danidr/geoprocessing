#!/bin/bash 

#module load GDAL/2.2.0-intel-2018

#cd /home/ddare/OneDrive/luckynet/Data/treeCover_hansen

#convert to binary raster, right now its NA not 1 0 

# YEAR=  $(seq 1992 2015 )

# scp downloaded_file/* ddare@pelican:/storepelican/ddare/ESA_CCI_processing/


for YEAR in {2000..2015} ;  do 
	#start outer loop
	echo $YEAR
	declare -a a_names=("10" "11" "12" "20" "30" "40" "50" "60" "61" "62" "70" "71" "72" "80" "81" "82" "90" "100" "110" "120" "121" "122" "130" "140" "150" "152" "153" "160" "170" "180" "190" "200" "201" "202" "210" "220")
	# a=($a)

	for ai in "${a_names[@]}"
	do
		echo Processing $ai
		b1=$(( ai-1 ))
		b2=$(( ai+1 ))
		pkgetmask -ot Byte -co COMPRESS=DEFLATE -co ZLEVEL=9 -min  $b1 -max $b2 -data 1 -nodata 0   -i downloaded/ESACCI-LC-L4-LCCS-Map-300m-P1Y-${YEAR}-v2.0.7.tif   -o disaggregated/LC_${YEAR}_${ai}.tif

	done

done #close outer loop





