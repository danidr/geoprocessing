#!/bin/bash

#module purge
#module load pktools
#module load old
#module load    GDAL/2.2.0-intel-2018 

#fold_numb=1
fold_numb=$1
#fold_numb={1..4}
for i in  disaggregated_flt/folder${fold_numb}/*.tif; do
 			in_name=`echo ${i} | cut -d '/' -f 3`
 			echo $in_name
 			ext_name=`echo ${i} | cut -d '/' -f 3 |  cut -f 1 -d '.'`
 			echo $ext_name
 			echo "avereging ${ext_name} to 1k spat res"
			gdalwarp -co "COMPRESS=LZW"  -srcnodata -9999 -dstnodata -9999 -tr 0.008333333300000 -0.008333333300000 -r average ${i} lc_average_1k/${ext_name}_avr_1k.tif
done
