#!/bin/bash
module purge
module load pktools

YEAR=$1
for i in disaggregated/LC_${YEAR}_*.tif; do
	in_name=`echo ${i} | cut -d '/' -f 2`
	echo "Counting LC class pixel within 1km2 for file" $i	
	pkfilter --co="COMPRESS=LZW" -i ${i} -o disaggregated_1k/${in_name}_1k.tif -f sum  -dx 3 -dy 3 -d 3 
	# gdal_translate -ot Int32 LC_2006_10_1k.tif LC_2006_10_1k_int.tif  
	# gdal_calc.py --co="COMPRESS=LZW" --overwrite -A LC_2006_10_1k_int.tif --outfile LC_2006_10_area_1k.tif --type='Float64' --calc="A * 0.09"
done 
