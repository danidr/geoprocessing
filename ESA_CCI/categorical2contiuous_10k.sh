#!/bin/bash

#module purge
#module load pktools
#module load old
#module load    GDAL/2.2.0-intel-2018 


#YEAR={2000..2015}
YEAR=$1
for i in disaggregated/LC_${YEAR}_*.tif; do
			in_name=`echo ${i} | cut -d '/' -f 2`
			echo $in_name
			echo "editing" ${in_name}
			gdal_edit.py  -a_nodata '-9999' disaggregated/${in_name}
			echo "coverting to Float32" ${in_name}
			ext_name=`echo ${i} | cut -d '/' -f 2 |  cut -f 1 -d '.'`
			gdal_translate -co "COMPRESS=LZW" -of GTIFF -ot Float32 -a_nodata '-9999' disaggregated/${in_name} disaggregated_flt/${ext_name}_flt.tif
			echo "avereging ${in_name} to 10k spat res"
			gdalwarp -co "COMPRESS=LZW"  -srcnodata -9999 -dstnodata -9999 -tr 0.08333333300000 -0.08333333300000 -r average disaggregated_flt/${ext_name}_flt.tif lc_average_10k/${ext_name}_flt_avr_10k.tif
done
